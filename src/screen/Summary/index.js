import React from "react";
import PropTypes from "prop-types";
import { View, Text, Button, StatusBar } from "react-native";
import Layout from "../../containers/Layout";
import { summaryStyle } from "../../styles/summary";
import WButton from "../../components/Button";
import { useSelector } from "react-redux";

export default function SummaryScreen({ navigation }) {
  const { water } = useSelector((state) => state);
  return (
    <View style={summaryStyle.container}>
      <View
        style={{
          ...summaryStyle.topContainer,
          height: `${100 - water.percentage}%`,
        }}
      ></View>
      <View
        style={{
          ...summaryStyle.bottomContainer,
          height: `${water.percentage}%`,
        }}
      ></View>
      <View style={summaryStyle.percentageContainer}>
        <Text style={summaryStyle.percentage}>{water.percentage}%</Text>
      </View>
      <WButton
        title={"Back to Edit Profile"}
        onPress={() => navigation.navigate("Profile")}
        buttonStyle={summaryStyle.backButtonFixedPosition}
      />
      <StatusBar barStyle="light-content" />
    </View>
  );
}

SummaryScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
